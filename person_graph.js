(function(){
	showPersonGraph = function(csvData){
		var pWidth = 175, pHeight = 341;
		var divWidth = 285, divHeight = 360;
		// left marginは左の目盛り分
		var margin = {top: 10, right: 80, bottom: 10, left: 30},
			width = divWidth - margin.left - margin.right,
			height = divHeight - margin.top - margin.bottom;
		// [0, pWidth]がグラフの横幅、0.5がpadding
		var x = d3.scale.ordinal().rangeRoundBands([0, pWidth], 0.5);
		var y = d3.scale.linear().rangeRound([0, height]);
		var xAxis = d3.svg.axis().scale(x).orient("bottom");
		var yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(d3.format(".2s"));

		d3.select("#pGraphDiv").remove();

		var svg = d3.select("#personGraphDiv")
			.append("div")
			.attr("id","pGraphDiv")
			.style({"position":"relative", "width":divWidth+"px", "height":divHeight+"px", "font":"12px sans-serif"})
			.append("svg")
			.attr("width", divWidth).attr("height", divHeight)
			.append("g")
			// グラフの描画領域を移動させる
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		d3.select("#pGraphDiv")
			.append("img")
			.attr("src", "./person2.png")
			.style({"position":"absolute", "left": margin.left + "px","top": margin.top + "px"});

		var data = d3.csv.parse(csvData);
		var colorArray = ["#fd8d3c", "#a1d99b", "#6baed6", "#9e9ac8", "#f7b6d2", ,"#fdd0a2", "#e7cb94", "#e6550d"];
		var count = 0;
		for(var i in data[0]){
			if(data[0].hasOwnProperty(i)) count++;
		}
		var slicedArray = colorArray.slice(0, count-1);
		slicedArray.push("#969696");
		var color = d3.scale.ordinal().range(slicedArray);
		color.domain(d3.keys(data[0]));
		// y軸の各値と合計値を求めている
		data.forEach(function(d) {
		var y0 = 0;
		d.proportions = color.domain().map(function(name) { return {"name": name, "y0": y0, "y1": y0 += +d[name]}; });
		d.total = d.proportions[d.proportions.length - 1].y1;
		});
		data.sort(function(a, b) { return b.total - a.total; });
		y.domain([0, data[0].total]);
		svg.append("g")
		  .attr("class", "y axis")
		  .call(yAxis)
		.append("text")
		  .attr("transform", "rotate(-90)")
		  .attr("y", 6)
		  .attr("dy", ".71em")
		  .style("text-anchor", "end")
		  .text("割合");

		//棒グラフの描画（細長い長方形を積み重ねる）
		var state = svg.selectAll(".state")
		  .data(data)
		.enter().append("g")
		  .attr("class", "g")
		  .attr("transform", function(d) { return "translate(" + x(d.State) + ",0)"; });
		state.selectAll("rect")
		  .data(function(d) { return d.proportions; })
		.enter().append("rect")
		  .attr("width", x.rangeBand())
		  .attr("y", function(d) { return y(d.y0); })
		  .attr("height", function(d) { return y(d.y1) - y(d.y0); })
		  .style("fill", function(d) {
			  if(d.name == "Other" || d.name == "その他")
				  return "#bdbdbd"
			  else
				  return color(d.name); 
			});

		// 判例
		var rectSize = 20;
		var rectPadding = 5;
		var legend = svg.selectAll(".legend").data(color.domain().slice())
			.enter().append("g")
		  	.attr("class", "legend")
		  	.attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

		legend.append("rect")
			.attr("x", pWidth + rectPadding)
			.attr("width", rectSize)
			.attr("height", rectSize)
			.style("fill", color);

		legend.append("text")
			.attr("x", pWidth + rectSize + rectPadding)
			.attr("y", 10)
			.attr("dy", ".35em")
			.text(function(d) { return d; });
		d3.selectAll(".axis, .line").style({"fill":"none","stroke":"#000"})
	}
})();
